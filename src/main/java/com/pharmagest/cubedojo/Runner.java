package com.pharmagest.cubedojo;

public class Runner {

    public static void main(String[] args) {
        new Runner().run();
    }

    private void run() {
        new MainForm();
    }
}
