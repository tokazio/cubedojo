package com.pharmagest.cubedojo;

import javax.swing.*;
import java.awt.*;

public class MainForm {

    MainForm() {
        JFrame f = new JFrame();
        f.setTitle("CubeDojo");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setLocationRelativeTo(null);
        JPanel p = new JPanel();
        p.setBackground(Color.BLACK);
        p.setPreferredSize(new Dimension(1024, 768));
        f.add(p);
        f.pack();
        f.setVisible(true);
    }
}
